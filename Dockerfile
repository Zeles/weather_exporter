FROM golang:1.13.0-buster

LABEL maintainer="Boris Platonov Zeles1000@gmail.com"

ENV OPENWEATHERAPI="cf52fea73efb249e6b37f98e27f6e07c" \
    TEMPUNIT="C" \
    WEATHERLANG="ru" \
    CITYIDS="524901,542374"

RUN apt update

RUN go get github.com/briandowns/openweathermap && go get github.com/prometheus/client_golang/prometheus

WORKDIR /app

COPY . .

RUN go build -o weather_exporter .

EXPOSE 7777

CMD ["./weather_exporter"]