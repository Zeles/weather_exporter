package main

import (
	"flag"
	owm "github.com/briandowns/openweathermap"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var addr = flag.String("listen-address", ":7777",
	"The address to listen on for HTTP requests.")

type Weather struct {
	Id        int     `json:"id"`
	City      string  `json:"city"`
	Temp      float64 `json:"temp"`
	Pressure  float64 `json:"pressure"`
	Humidity  int     `json:"humidity"`
	WindSpeed float64 `json:"wind_speed"`
}

type Client struct {
	Ids           []int
	CityTemp      *prometheus.GaugeVec
	CityPressure  *prometheus.GaugeVec
	CityHumidity  *prometheus.GaugeVec
	CityWindSpeed *prometheus.GaugeVec
}

func GetWeather(id int) (*Weather, error) {
	weath, err := owm.NewCurrent(os.Getenv("TEMPUNIT"), os.Getenv("WEATHERLANG"), os.Getenv("OPENWEATHERAPI"))
	if err != nil {
		return nil, err
	}

	err = weath.CurrentByID(id)
	if err != nil {
		return nil, err
	}
	w := Weather{Id: id, City: weath.Name, Temp: weath.Main.Temp, Pressure: weath.Main.Pressure,
		Humidity: weath.Main.Humidity, WindSpeed: weath.Wind.Speed}
	return &w, nil
}

func GetWeathers(ids []int) []Weather {
	var weath []Weather
	for _, id := range ids {
		temp, err := GetWeather(id)
		if err != nil {
			log.Print(err)
		}
		if temp != nil {
			weath = append(weath, *temp)
		}
	}
	return weath
}

func (weather *Weather) Update() {
	weather, err := GetWeather(weather.Id)
	if err != nil {
		log.Print(err)
	}
}

func (c *Client) wether() {
	for {
		weathers := GetWeathers(c.Ids)
		for _, weather := range weathers {
			c.CityTemp.WithLabelValues(weather.City).Set(weather.Temp)
			c.CityPressure.WithLabelValues(weather.City).Set(weather.Pressure)
			c.CityHumidity.WithLabelValues(weather.City).Set(float64(weather.Humidity))
			c.CityWindSpeed.WithLabelValues(weather.City).Set(weather.WindSpeed)
		}
		time.Sleep(5 * time.Second)
	}
}

func getweatherids() ([]int, error) {
	var ids []int
	strids := strings.Split(os.Getenv("CITYIDS"), ",")
	for _, strid := range strids {
		id, err := strconv.Atoi(strid)
		if err != nil {
			return nil, err
		}
		ids = append(ids, id)
	}
	return ids, nil
}

func (c *Client) init() {
	c.CityTemp = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "cityTemp",
		},
		[]string{"city"},
	)
	c.CityPressure = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "cityPressure",
		},
		[]string{"city"},
	)
	c.CityHumidity = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "cityHumidity",
		},
		[]string{"city"},
	)
	c.CityWindSpeed = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "cityWindSpeed",
		},
		[]string{"city"},
	)
	ids, err := getweatherids()
	if err != nil {
		log.Panic(err)
	}
	c.Ids = ids
}

func main() {
	client := new(Client)
	client.init()
	go client.wether()

	prometheus.MustRegister(client.CityTemp)
	prometheus.MustRegister(client.CityPressure)
	prometheus.MustRegister(client.CityHumidity)
	prometheus.MustRegister(client.CityWindSpeed)

	http.Handle("/metrics", promhttp.Handler())

	log.Printf("Starting web server at %s\n", *addr)
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Printf("http.ListenAndServer: %v\n", err)
	}
}
