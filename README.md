Weather exporter
=============================

Exporter to retrieve data from openweatherapi

Docker:

    docker build -t weather .
    
    docker run -e "OPENWEATHERAPI="api_key"" -e "TEMPUNIT="C"" \
               -e "WEATHERLANG="ru"" -e "CITYIDS="524901,542374"" -p 7777:7777 -d weather